<?php

/*

	Template Name: Marketplace

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="marketplace">
			<div class="wrapper">

				<div class="section-wrapper">

					<div class="section-header">
						<h1><?php the_title(); ?></h1>

						<div class="description">
							<?php the_field('description'); ?>
						</div>
					</div>

					<section id="shop">
						<?php if(have_rows('shop')): while(have_rows('shop')) : the_row(); ?>

							<div class="shop-item">
								<div class="photo">
									<a href="<?php the_sub_field('url'); ?>" rel="external">
										<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>		
								</div>

								<div class="info">
									<a href="<?php the_sub_field('url'); ?>" rel="external" class="title">
										<?php the_sub_field('title'); ?>
									</a>
								</div>

							</div>
						 
						<?php endwhile; endif; ?>

						<?php if(get_field('show_coming_soon')): ?>

							<div class="shop-item coming-soon">
								<div class="photo">
									<div class="content">
										<span>Stay tuned for more.</span>
									</div>
								</div>

							</div>

						<?php endif; ?>
					</section>

				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>