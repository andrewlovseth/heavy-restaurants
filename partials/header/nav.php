<?php if(have_rows('navigation','options')): ?>
	<div class="main-nav">
		<?php while(have_rows('navigation', 'options')): the_row(); ?>
 
			<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

		<?php endwhile; ?>
	</div>
<?php endif; ?>