<div class="status">

	<?php get_template_part('partials/header/logo'); ?>

	<div class="message">
		<div class="headline">
			<h2><?php the_field('status_headline', 'options'); ?></h2>
		</div>

		<div class="copy">
			<?php the_field('status_copy', 'options'); ?>
		</div>
	</div>

</div>