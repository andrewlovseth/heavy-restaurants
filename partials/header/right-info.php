<div class="info right">
	<?php if(get_field('location_header', 'option')): ?>
		<h3><?php the_field('location_header', 'option'); ?></h3>
	<?php else: ?>
		<h3>Location</h3>
	<?php endif; ?>
	<?php the_field('location', 'options'); ?>
</div>