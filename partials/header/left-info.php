<div class="info left">
	<?php if(get_field('hours_header', 'option')): ?>
		<h3><?php the_field('hours_header', 'option'); ?></h3>
	<?php else: ?>
		<h3>Hours</h3>
	<?php endif; ?>

	<?php the_field('hours', 'options'); ?>
</div>