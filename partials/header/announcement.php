<?php

$home = get_page_by_path( 'home' );

if(get_field('show_announcement', $home->ID)): ?>

	<style>

		<?php if(get_field('announcement_background_color', $home->ID)): ?>
			#announcement {
				background-color: <?php the_field('announcement_background_color', $home->ID); ?> !important;
				color: <?php the_field('announcement_text_color', $home->ID); ?> !important;
			}
		<?php endif; ?>

		<?php if(get_field('announcement_text_color', $home->ID)): ?>
			#announcement a,
			#announcement p {
				color: <?php the_field('announcement_text_color', $home->ID); ?> !important;
			}
		<?php endif; ?>

	</style>

	<section id="announcement" class="top-banner">
		<div class="wrapper">

			<div class="announcement-wrapper">
				<?php the_field('announcement', $home->ID); ?>
			</div>

		</div>
	</section>

<?php endif; ?>
