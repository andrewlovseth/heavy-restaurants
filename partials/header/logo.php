<div class="logo">
	<div class="image">
		<a href="<?php echo site_url('/'); ?>">
			<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="mobile-menu-btn">
		<a href="#" id="toggle">
			<div class="patty"></div>
		</a>
	</div>

	<nav class="mobile">
		<?php get_template_part('partials/header/nav'); ?>
	</nav>
</div>