<?php if(get_field('show_heavy_at_home', 'options')): ?>

	<section id="heavy-at-home">
		<div class="overlay">
			<div class="overlay-wrapper">
				
				<a href="#" class="close">✕</a>

				<div class="info">
					<div class="info-wrapper">

						<div class="headline">
							<h1><?php the_field('heavy_at_home_headline', 'options'); ?></h1>
						</div>

						<div class="copy">
							<p><?php the_field('heavy_at_home_copy', 'options'); ?></p>
						</div>

						<?php 
							$link = get_field('heavy_at_home_cta', 'options');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						 ?>

						 	<div class="cta">
						 		<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						 	</div>

						<?php endif; ?>

					</div>
				</div>

			</div>
		</div>
	</section>

<?php endif; ?>