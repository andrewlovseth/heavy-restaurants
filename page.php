<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="standard">
			<div class="wrapper">

				<div class="section-header">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="section-body">
					<?php the_content(); ?>
				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>