<?php

/*

	Template Name: Location

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="location">
			<div class="wrapper">

				<div class="section-wrapper">

					<div class="section-body">

						<div class="info">
							<div class="section-header">
								<h1><?php the_title(); ?></h1>
							</div>

							<?php the_content(); ?>
						</div>

						<div class="photo">
							<img src="<?php $image = get_field('exterior_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="map">
							<a href="<?php the_field('map_link'); ?>" rel="external">
								<img src="<?php $image = get_field('map_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

					</div>

				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>