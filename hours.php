<?php

/*

	Template Name: Hours

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="standard">
			<div class="wrapper">

				<div class="section-wrapper">

					<div class="section-body">
						<div class="hours">
							<div class="section-header">
								<h1><?php the_title(); ?></h1>
							</div>

							<?php the_content(); ?>
						</div>


					</div>

				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>