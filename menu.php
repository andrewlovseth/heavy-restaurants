<?php

/*

	Template Name: Menu

*/

get_header(); ?>

	<section class="menu">
		<div class="wrapper">

			<div class="section-wrapper">

				<div class="section-header">
					<h1><?php the_title(); ?></h1>
				</div>

				<section id="menu-list">
					<?php if(have_rows('menus')): while(have_rows('menus')) : the_row(); ?>

						<div class="menu-item">
							<div class="photo">
								<div class="content">
									<a href="<?php the_sub_field('pdf_url'); ?>" rel="external">
										<img class="menu-photo" src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

										<span class="pdf"><img src="<?php bloginfo('template_directory') ?>/images/pdf.svg" alt="PDF" /></span>
									</a>	
								</div>	
							</div>

							<div class="info">
								<a href="<?php the_sub_field('pdf_url'); ?>" rel="external" class="title">
									<?php the_sub_field('title'); ?>
								</a>
							</div>

						</div>
					 
					<?php endwhile; else: ?>

						<div class="menu-item no-menus">
							<div class="info">
								<p>Coming Soon</p>
							</div>
						</div>

					<?php endif; ?>
				</section>

			</div>

		</div>
	</section>

<?php get_footer(); ?>