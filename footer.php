<?php include(locate_template('partials/header/global-variables.php')); ?>

	<footer>
		<div class="wrapper">

			<?php get_template_part('partials/footer/newsletter'); ?>
			
			<?php get_template_part('partials/footer/hrg'); ?>

		</div>
	</footer>

	<?php get_template_part('partials/footer/newsletter-modal'); ?>

	<?php get_template_part('partials/footer/heavy-at-home-modal'); ?>

	<?php include(locate_template('partials/footer/scripts.php')); ?>

	<?php wp_footer(); ?>

</body>
</html>