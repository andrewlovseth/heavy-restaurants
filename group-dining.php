<?php

/*

	Template Name: Group Dining

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="group-dining">
			<div class="wrapper">

				<div class="section-header">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="gallery">
					<?php  $images = get_field('gallery'); if( $images ): ?>
						<?php foreach( $images as $image ): ?>
							<div class="photo">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>							
						<?php endforeach; ?>
					<?php endif; ?>
				</div>

				<div class="copy">
					<?php the_field('copy'); ?>
				</div>

				<div class="contact-info">
					<?php the_field('contact_info'); ?>

					<div class="brochure">
						<a href="<?php the_field('menus_pdf'); ?>" class="cta" rel="external"><?php the_title(); ?> Menus</a>
					</div>
				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>