<?php include(locate_template('partials/header/global-variables.php')); ?>
<!DOCTYPE html>
<html>
<head>
	<?php the_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php get_template_part('partials/head/styles'); ?>
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<?php the_field('body_meta', 'options'); ?>

	<?php get_template_part('partials/header/utility-nav'); ?>

	<?php get_template_part('partials/header/announcement'); ?>

	<div class="header-bg">

		<nav class="desktop">
			<div class="wrapper">

				<?php get_template_part('partials/header/nav'); ?>
				<?php get_template_part('partials/header/social'); ?>

			</div>
		</nav>

		<?php get_template_part('partials/header/main-header'); ?>

		<section class="call-to-actions desktop">
			<div class="wrapper">

				<?php get_template_part('partials/header/ctas'); ?>

			</div>
		</section>

	</div>