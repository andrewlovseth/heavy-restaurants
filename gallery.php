<?php

/*

	Template Name: Gallery

*/

get_header(); ?>


	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="gallery">
			<div class="wrapper">

				<div class="section-wrapper">

					<div class="section-body">
						<div class="gallery">
							<?php $photoIDs = ''; $images = get_field('gallery'); if( $images ): ?>
								<?php foreach( $images as $image ): ?>
									
									<?php $photoIDs .= $image['ID'] . ','; ?>
		
								<?php endforeach; ?>
							<?php endif; ?>

							<?php echo do_shortcode('[gallery ids="' . $photoIDs . '"]'); ?>
						</div>
					</div>

				</div>

			</div>
		</section>

	<?php endwhile; endif; ?>


<?php get_footer(); ?>